import { Negociacao } from "./negociacao.js";

export class Negociacoes {
    private negociacoes: Negociacao [] = [];
    // private negociacoes: Array<Negociacao> = [];     Generics mais parecido com Java

    adiciona(negociacao: Negociacao) {
        this.negociacoes.push(negociacao);
    }

    // lista(): ReadonlyArray<Negociacao> {        Generics mais parecido com Java
    lista(): readonly Negociacao [] {
        return this.negociacoes;
    }

}

