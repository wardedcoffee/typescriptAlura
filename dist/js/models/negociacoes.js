export class Negociacoes {
    constructor() {
        this.negociacoes = [];
    }
    // private negociacoes: Array<Negociacao> = [];     Generics mais parecido com Java
    adiciona(negociacao) {
        this.negociacoes.push(negociacao);
    }
    // lista(): ReadonlyArray<Negociacao> {        Generics mais parecido com Java
    lista() {
        return this.negociacoes;
    }
}
